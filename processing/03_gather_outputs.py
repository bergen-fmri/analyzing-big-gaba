#!/usr/bin/python3

from glob import glob;
import pandas as pd;
import numpy as np;
import os;
import sys;

merged=None;

site_folders=glob('/data/big_gaba/[PGS]*');

print(sys.argv);

def indexify(df):
  ix=['%s_%s_%s_%s_%s' % (a,b,c,d,e) for a,b,c,d,e in zip(df['spec'],df['label'],df['target'],df['site'],df['subject'])];
  df['ix_sltss']=ix;
  df=df.set_index('ix_sltss');
  return df;

def update_append(d1,d2):

  #dataframe.merge renames columns, which we don't want.
  #dataframe.update only handles left join (updating existing records), which only solves part of the problem
  #so do an update, then an append for anything missing
  #merged=merged.merge(site_data,left_index=True,right_index=True,how='outer');

  if d1 is None:
    return pd.DataFrame(d2);

  # add any new rows from d2 into d1
  d1=d1.append(d2.loc[[x not in d1.index for x in d2.index]]); # add any new records...

  # add any new columns from d2 into d1, such that these are included in the update (only an issue if nothing was appended)
  for c in d2.columns:
    if not c in d1:
      d1[c]=np.nan;

  d1.update(d2,join='left',overwrite=True);  # update any pre-existing records; in place by definition, only supports left join

  return d1;

if len(sys.argv)>1:
  for preload in sys.argv[1:]:
    if preload=='latest': # find the most recent .csv, use that to preload data
      preload=None;
      merged_outputs=glob('merged*csv');

      if len(merged_outputs)>0:
        merged_outputs.sort(key=lambda x: os.path.getmtime(x),reverse=True);
        latest=merged_outputs[0];
        preload=latest;

    if preload is None:
      continue;

    data=indexify(pd.read_csv(preload,skipinitialspace=True,index_col=0));
    print(data['label'].unique());
    merged=update_append(merged,data);

#print(merged);
#print(merged['label'].unique());

from datetime import datetime;

for site_folder in site_folders:
  print(site_folder);
  site_outputs=glob(os.path.join(site_folder,'output*csv'));

  if len(site_outputs)>0:

    site_outputs.sort(key=lambda x: os.path.getmtime(x),reverse=True);

    for fn in site_outputs: # go back in time until we find a non-empty output
      try:
        site_data=pd.read_csv(fn,skipinitialspace=True);
        break;
      except pd.errors.EmptyDataError:
        site_data=None;
        print('%s CONTAINS NO DATA!' % fn);
        continue;

    if site_data is None:
      continue;

    n1=len(site_data);
    site_data=indexify(site_data.drop_duplicates(subset=['spec','label','target','site','subject']));
    n2=len(site_data);

    if (n2<n1):
      print('%s: %d records, %d unique (%d DUPLICATES)' % ( fn,n1,n2,n1-n2))
    else:
      print('%s: %d records' % (fn,n1));

    debug_merge=False;

    merged=update_append(merged,site_data);

    #print(merged);
    dedup=merged.drop_duplicates(subset=['spec','label','target','site','subject']);
    if len(dedup)<len(merged):
      print('%s introduced non-unique values' % (fn,));
      merged=dedup;


print(merged);
ofn='merged.%s.csv' % (datetime.now().strftime('%Y%m%d.%H%M'),);
merged.to_csv(ofn);

print(site_folders);

print('Columns:');
cs=sorted(list(set(merged.columns)));
for c in [cs[i:i+5] for i in range(0,len(cs),5)]:
  print(' '.join(['%-20s' % (x[:20],) for x in c]));

# spec, label, target, sequence, site
for c in ['spec','label','target','sequence','site','subject']:
  print('%19s: %s' % (c,' '.join([str(x) for x in merged[c].unique()])));

labels=merged['label'].unique();

for x in merged['site'].unique():
  ss=merged[merged['site']==x]['subject'].unique();
  exceptions=[];
  for lab in labels:
    #ssl=merged[np.logical_and(merged['site']==x,merged['label']==lab)]['subject'].unique();
    subspecs=list(set(merged[np.logical_and(merged['site']==x,merged['label']==lab)]['spec'].unique()));
    if len(subspecs)>0:
      for subspec in subspecs:
        ssl=merged[np.logical_and(merged['site']==x,np.logical_and(merged['label']==lab,merged['spec']==subspec))]['subject'].unique();
        if len(ssl)<len(ss):
          exceptions.append('%s %s: %d' % (lab,subspec,len(ssl)));
    else:
      exceptions.append('%s : nothing' % (lab,));

  if len(exceptions)>0:
    exstr=' (except: %s)' % (', '.join(exceptions));
  else:
    exstr='';

  print('%s : %d%s' % (x,len(ss),exstr));
