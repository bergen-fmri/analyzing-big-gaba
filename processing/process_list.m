function ret=process_list(subs)

  mode='MS1b';

  quick=1;
  dry=0;
  stop_on_error=0;
  enable_caching=0;

  include_noMM=1;

  if (strncmp(mode,'MS1',3))
    output_prefix=[];
    enable_caching=1;
  else
    error('Unknown mode');
  end

  v=version('-release');
  year=str2num(v(1:4));
  if (year<2015)
    % datetime function, introduced in 2014b, is required for the siemens loader
    error('Your MATLAB release is quite ancient; this might not work out particularly well.');
  end

  [folder,base,ext]=fileparts(mfilename('fullpath'));
  [pfolder,pb,pe]=fileparts(folder);

  setup_submodules();

  output_per_site=1;


  include_gannet_preproc=0;               % XX
  include_fida_preproc=1;

  include_bl_exploration=0; % 202105
  include_mm_exploration=0; % 202105
  extended_mm_exploration=0;

  include_osprey_separate=1;

  if include_mm_exploration
    include_osprey_separate_MM_1to1GABA=1;
    include_osprey_separate_MM_3to2MM09=1;
    include_lcmodel_MM_3to2MM09=1;
    include_lcmodel_MM_GABA_MM09_ART=1;
  else
    include_osprey_separate_MM_1to1GABA=0; % 202105
    include_osprey_separate_MM_3to2MM09=0;
    include_lcmodel_MM_3to2MM09=0;
    include_lcmodel_MM_GABA_MM09_ART=0;
  end

  include_osprey_concat=0;                % XX
  include_osprey_fida_preproc=1;          % XX
  include_osprey_native_preproc=0;        % XX
  include_osprey_fida_preproc_concat=0;   % XX

  include_tarquin=0;
  include_gannet=0;
  include_lcmodel=0;
  include_jmrui=0;
  include_osprey=0;
  include_fsl_mrs=0;

  if quick
    include_fsl_mrs=0;
    include_lcmodel=0;
    include_tarquin=0;
    include_gannet=0;
    include_jmrui=0;
    include_osprey=0;
    include_fsl_mrs_noMM_MH=1;
  end

  include_lcmodel_fancy=0;
  include_lcmodel_universal=0;

  % these use standard basis-set
  include_lcmodel_noMM=include_noMM;
  include_lcmodel_MM_1to1GABA=1;

  include_tarquin_stdbasis_off=1; % gets hung up on some Siemens spectra

  include_fsl_mrs_noMM=include_noMM;

  include_fsl_mrs_MH=1;
  include_fsl_mrs_Grouped=0;

  include_tarquin_noMM=include_noMM;

  include_standard_basis=1;


  all_results=datatable_ARC();
  dn=pwd

  output_filename=fullfile(dn,sprintf('output.%s.csv', datestr(now,'yyyymmdd.HHMM')))

  owd=pwd;
  last_site=[];

  for i=1:length(subs)
    close all;

    % Some of our submodules leak filehandles.
    % Osprey LogFile handles are not closed reliably
    % Philips SPAR files (and possibly others are not closed reliably by Gannet
    % Just close everything which is still open:
    x=fclose('all');

    % fIDs = fopen('all');
    % for i = 1:numel(fIDs)
    %    fName = fopen(fIDs(i));
    %    disp(sprintf('%d: %s', fIDs(i), fName));
    % end

    % check for lingering figure handles (should be none):

    figHandles = findall(groot, 'Type', 'figure');

    if numel(figHandles)>0
      warning('Some figure handles were not closed correctly.');
    end

    ret=[];
    java.lang.System.gc();

    % and yet, something leaks memory.
    % osprey/libraries/L-BFGS-B-C/Matlab/lbfgsb.m has persistent things. is it that?
    % https://www.ibm.com/developerworks/library/j-jtp01274/

    this_set=subs{i}

    if iscell(this_set)
      this_main=this_set{1}
    else
      this_main=this_set
    end

    if include_osprey
      % Osprey adjusts the matlab path, and forces its version of FID-A upon us. Regain control of this...
      force_FIDA('compat'); % use standard upstream (ish) FID-A
    end

    LCM_results=[];
    [mb,mn,me]=fileparts(this_main);
    [site_folder,subject_name,mbe]=fileparts(mb);      % mbb: site folder, mbn: subject name

    [mbbb,site_name,mbbe]=fileparts(site_folder);  % mbbb: top-level folder; mbbn: site folder name

    if ~isempty(output_prefix)
      site_folder=fullfile(mbbb,output_prefix,site_name)
      if ~exist(site_folder,'dir')
        if ~exist(fullfile(mbbb,output_prefix),'dir')
          mkdir(fullfile(mbbb,output_prefix));
        end
        mkdir(site_folder);
      end
    end

    cd(site_folder);

    if isempty(last_site) || ~strcmp(site_name,last_site) % new site
      if output_per_site>0
        all_results=datatable_ARC();
        output_filename=fullfile(site_folder,sprintf('output.%s.csv', datestr(now,'yyyymmdd.HHMM')))
        last_site=site_name;
      end
    end

    site_name
    switch(site_name(1))
      case 'G'
        std_basis_vendor='ge';
      case 'P'
        std_basis_vendor='philips';
      case 'S'
        std_basis_vendor='siemens';
      otherwise
        error('Unrecognised vendor')
    end

    std_basis_vendor

    %%%%% Initial run : Gannet for loading
    try % process this subject within a catch-all exception handing block {{{

      sprintf('Processing: "%s"',this_main)

      jobs={};

      if include_gannet || include_jmrui || include_fida_preproc || include_osprey_fida_preproc
        jobs{end+1}='gannetload';
      end

      if include_gannet
        jobs{end+1}='Gannet';
      end

      if include_standard_basis
        if include_fsl_mrs_noMM_MH
          jobs{end+1}='FSLMRS-fix-MH-noMM';
        end
        if include_fsl_mrs
          jobs{end+1}='FSLMRS-fix';
          if include_fsl_mrs_noMM
            jobs{end+1}='FSLMRS-fix-noMM';
          end
          if include_fsl_mrs_Grouped
            jobs{end+1}='FSLMRS-Grouped';
            jobs{end+1}='FSLMRS-GlxGroup';
          end
          if include_fsl_mrs_MH
            jobs{end+1}='FSLMRS-fix-MH';
          end
        end
        if include_lcmodel
          jobs{end+1}='LCModel-stdbasis';
          if include_bl_exploration
            jobs{end+1}='LCModel-stdbasis-bl06';
          end
        end
        if include_tarquin
          jobs{end+1}='Tarquin-stdbasis';
          if include_tarquin_noMM
            jobs{end+1}='Tarquin-noMM';
          end
        end
        % FIXME! add tarquin
      end


      if include_gannet_preproc && include_lcmodel
        jobs{end+1}='Gannet-LCModel';
      end

      if include_gannet_preproc && include_tarquin
        jobs{end+1}='Gannet-Tarquin';
      end

      if include_fida_preproc
        if include_gannet
          jobs{end+1}='FIDA-Gannet';
        end
        if include_lcmodel % {{{
          jobs{end+1}='LCModel';
          if include_lcmodel_fancy
            jobs{end+1}='LCModel-fancy';
          end
          if include_lcmodel_universal
            jobs{end+1}='LCModel-universal';
          end
          if include_lcmodel_noMM
            jobs{end+1}='LCModel-noMM';
            if include_bl_exploration
              jobs{end+1}='LCModel-noMM-bl06';
            end
          end
          if include_lcmodel_MM_1to1GABA
            jobs{end+1}='LCModel-MM_1to1GABA';
          end
          if include_lcmodel_MM_3to2MM09
            jobs{end+1}='LCModel-MM_3to2MM09';
          end
          if include_lcmodel_MM_GABA_MM09_ART
            jobs{end+1}='LCModel-MM_GABA_MM09_ART';
          end
        end % include_lcmodel }}}
      end

      if include_tarquin
        jobs{end+1}='Tarquin';
      end

      if include_osprey
        jobs{end+1}='osprey_init';

        osprey_suf='';
        osprey_ver=arc_osprey_release();
        osprey_suf=sprintf('-%s',osprey_ver);

        if include_osprey_native_preproc % {{{
          jobs{end+1}=['Osprey-native-sep' osprey_suf];
          if include_osprey_separate_MM_1to1GABA
            jobs{end+1}=['Osprey-native-1to1GABAsoft-bl06' osprey_suf];
          end
          if include_osprey_concat
            jobs{end+1}=[ 'Osprey-native-concat' osprey_suf];
          end
        end % }}}

        if include_osprey_fida_preproc
          if include_osprey_fida_preproc_concat
            jobs{end+1}=[ 'FIDA-Osprey-concat-X1' osprey_suf];
          end

          if include_osprey_separate

            if include_noMM || include_bl_exploration
              jobs{end+1}=[ 'Osprey-sep' osprey_suf ];
            end

            jobs{end+1}=[ 'Osprey-sep-MM3' osprey_suf ]; % freeGauss14
            jobs{end+1}=[ 'Osprey-sep-MM3-freeGauss' osprey_suf ]; % freeGauss

            if include_bl_exploration
              jobs{end+1}=[ 'Osprey-sep-bl06' osprey_suf ];
            end

            if include_osprey_separate_MM_1to1GABA
              jobs{end+1}=['Osprey-1to1GABAsoft-bl06' osprey_suf ];
              if include_bl_exploration
                jobs{end+1}=['Osprey-1to1GABAsoft-bl04' osprey_suf ];
              end
            end

            if include_osprey_separate_MM_3to2MM09
              jobs{end+1}=[ 'Osprey-3to2MMsoft-bl06' osprey_suf ];
              if include_bl_exploration
                jobs{end+1}=['Osprey-3to2MMsoft-bl04' osprey_suf ];
              end
            end

          end

        end

        jobs{end+1}='osprey_done';

      end

      jobs

      pause(3)

      clear('g');
      clear('jm');
      clear('preprocessed');
      clear('preprocessed0');

      ji=0;
      while (ji<length(jobs))
        ji=ji+1;
        jj=jobs{ji};


        lab=jj;

        if (dry)
          continue;
        end

        cd(site_folder);
        close all;

        try
          switch jj
            case 'gannetload'
              g=MRS_interconnect_Gannet(this_main) % use GannetLoad, which is a little more capable than FID-A's IO functions
              ret=g; % return MRS_interconnect_Gannet
              warning('on'); % Gannet sometimes hides useful warnings. Undo this.
            case 'Gannet'
              % inherits default 'Gannet' label from the loader above
              Gannet_results=g.wrap();
              all_results.append(Gannet_results);
            case 'FSLMRS-fix'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor));
              clear('fm');
            case 'FSLMRS-fix-noMM'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('noMM',1,'basis_vendor',std_basis_vendor));
              clear('fm');
            case 'FSLMRS-Grouped'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              %all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH NAA+NAAG GABA+MM3co MM09ex','spec','diff'));
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH+GABA+MM3co NAA+NAAG MM09ex','spec','diff'));
              clear('fm');
            case 'FSLMRS-GlxGroup'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'metab_groups','Glu+Gln+GSH','spec','diff'));
              clear('fm');
            case 'FSLMRS-fix-MH'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('basis_vendor',std_basis_vendor,'algo','MH','spec','diff'));
              clear('fm');
            case 'FSLMRS-fix-MH-noMM'
              fm=MRS_interconnect_FSLMRS(preprocessed,'label',lab);
              all_results.append(fm.wrap('noMM',1,'basis_vendor',std_basis_vendor,'algo','MH','spec','diff'));
              clear('fm');
            case 'jMRUI'
              jm=MRS_interconnect_jMRUI(g,'label',lab)
              jm.cache_read();
              if ~jm.is_preprocessed()
                jm.preprocess();
                if (enable_caching)
                  jm.cache_write(); 
                end
              end
              jm.compare(g); % this exports a thing called comparison_metab
              if include_jmrui
                jm.write();
              end

              % record basic SNR/linewidth metrics from FIDA preproc
              preproc_info=jm.getQualityInfo(); % 'spec','diff'
              preproc_info.show();

              all_results.append(preproc_info);

              ret=jm; % return MRS_interconnect_jMRUI
              preprocessed0=jm;
              preprocessed=preprocessed0;

              if ~include_gannet_preproc
                clear('g'); % won't be needing this one anymore.
              end
              clear('jm','preprocessed0');
            case 'Gannet-LCModel'
              l=MRS_interconnect_LCModel(g,'label',lab);
              LCM_results=l.wrap();
              LCM_results.show();
              all_results.append(LCM_results);
              all_results.append(l.internal_quantify_water());
              clear('l');
            case 'Gannet-Tarquin'
              t=MRS_interconnect_tarquin(g,'label',lab);
              TQ_results=t.wrap();
              TQ_results.show();
              all_results.append(TQ_results);
              all_results.append(t.internal_quantify_water());
              clear('t');
            case 'FIDA-Gannet'
              g2=MRS_interconnect_Gannet(preprocessed,'label',lab);
              all_results.append(g2.wrap());
              clear('g2');
            case 'LCModel'
              l2=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l2.wrap());
              all_results.append(l2.internal_quantify_water());
              clear('l2');
            case 'LCModel-fancy'
              l3=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l3.wrap('fancy_baseline',1));
              clear('l3');
            case 'LCModel-universal'
              l4=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l4.wrap('universal_basis',1));
              clear('l4');
            case 'LCModel-stdbasis'
              l4b=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l4b.wrap('basis_vendor',std_basis_vendor));
              clear('l4b');
            case 'LCModel-noMM'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','noMM',1,'basis_vendor',std_basis_vendor));
              clear('l7');
            case 'LCModel-stdbasis-bl06'
              l4b=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l4b.wrap('basis_vendor',std_basis_vendor,'dkntmn',.6));
              clear('l4b');
            case 'LCModel-noMM-bl06'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','noMM',1,'dkntmn',.6,'basis_vendor',std_basis_vendor));
              clear('l7');
            case 'LCModel-MM_1to1GABA'
              l5=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l5.wrap('spec','diff','constraint_model','1to1GABAsoft','basis_vendor',std_basis_vendor));
              clear('l5');
            case 'LCModel-MM_3to2MM09'
              l6=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l6.wrap('spec','diff','constraint_model','3to2MMsoft','basis_vendor',std_basis_vendor));
              clear('l6');
            case 'LCModel-MM_GABA_MM09_ART'
              l7=MRS_interconnect_LCModel(preprocessed,'label',lab);
              all_results.append(l7.wrap('spec','diff','constraint_model','GABA_MM09_ART','basis_vendor',std_basis_vendor));
              clear('l7');
            case 'Tarquin'
              t2=MRS_interconnect_tarquin(preprocessed,'label',lab);
              all_results.append(t2.wrap());
              all_results.append(t2.internal_quantify_water());
              clear('t2');
            case 'Tarquin-stdbasis'
              t3=MRS_interconnect_tarquin(preprocessed,'label',lab);
              % can be quite slow with more complicated basis sets... optionally disable edit-off
              all_results.append(t3.wrap('spec','diff','basis_vendor',std_basis_vendor));
              if include_tarquin_stdbasis_off
                all_results.append(t3.wrap('spec','off','basis_vendor', std_basis_vendor));
              end
              clear('t3');
            case 'Tarquin-noMM'
              t3=MRS_interconnect_tarquin(preprocessed,'label',lab);
              all_results.append(t3.wrap('spec','diff','noMM',1,'basis_vendor',std_basis_vendor));
              clear('t3');

            % Osprey native (just sep and 1to1 bl06 now) {{{
            case [ 'Osprey-native-sep' osprey_suf ]
              os=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(os.wrap('fit_style','Separate'));
              clear('os');   % osprey struct gets huge!
            case ['Osprey-native-1to1GABAsoft-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.6));
              clear('os');
            case ['Osprey-native-concat' osprey_suf ]
              o=MRS_interconnect_Osprey(this_main,'label',lab);
              all_results.append(o.wrap());
              clear('o');
            % }}}

            % Osprey with FID-A preproc {{{

            case [ 'FIDA-Osprey-concat-X1' osprey_suf ]
              fo=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fo.wrap());
              clear('fo'); 

            case ['Osprey-sep' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate'));
              clear('fos');

            case ['Osprey-sep-MM3' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','freeGauss14'));
              clear('fos'); 

            case ['Osprey-sep-MM3-freeGauss' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','freeGauss'));
              clear('fos'); 

            case ['Osprey-sep-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','bLineKnotSpace',0.6));
              clear('os');   

            case ['Osprey-1to1GABAsoft-bl04' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.4));
              clear('os');   

            case ['Osprey-1to1GABAsoft-bl06' osprey_suf ]
              fos=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(fos.wrap('fit_style','Separate','coMM3','1to1GABAsoft','bLineKnotSpace',0.6));
              clear('fos');   

            case [ 'Osprey-3to2MMsoft-bl06' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','3to2MMsoft','bLineKnotSpace',0.6));
              clear('os');   

            case [ 'Osprey-3to2MMsoft-bl04' osprey_suf ]
              os=MRS_interconnect_Osprey(preprocessed,'label',lab);
              all_results.append(os.wrap('fit_style','Separate','coMM3','3to2MMsoft','bLineKnotSpace',0.4));
              clear('os');   

            % }}}

            case 'osprey_init' % special case: switch to Osprey's local FID-A variant
              force_FIDA('osprey'); % switch to Osprey's local FID-A version

            case 'osprey_done' % special case: switch back to compatible FID-A version
              force_FIDA('compat');  % use standard upstream (ish) FID-A

            otherwise
              warning(['Unknown job: ' jj]);
          end
          warning('on'); % Some tools (eg: gannet) occasionally hide useful warnings. make sure things are not hidden.
        catch Ex
          warning('on');
          ex_formatted=formatting.format_exception(Ex);
          %failures(files{i})=ex_formatted;
          warning(ex_formatted);
          warning([jj ' failed for ' this_main]);
          switch jj
            case 'gannetload'
              warning('Initial load failed; cannot proceed');
              rethrow(Ex);
            case 'jMRUI'
              warning('Initial load failed; cannot proceed');
              rethrow(Ex);
            otherwise
              if stop_on_error
                rethrow(Ex);
              end
          end
        end
      end

    % }}}
    catch Ex
      ex_formatted=formatting.format_exception(Ex);
      %failures(files{i})=ex_formatted;
      warning(ex_formatted);
      warning(['Processing failed for ' this_main]);
      if stop_on_error
        rethrow(Ex)
      end
    end

    cd(owd);

    try % write out results {{{
      disp('Results for all scans:');
      all_results.show();

      disp(['Saving results to: ' output_filename]);
      all_results.save(output_filename);
      % }}}
    catch Ex % no, writing did not work. {{{
      ex_formatted=formatting.format_exception(Ex);
      %failures(files{i})=ex_formatted;
      if stop_on_error
        rethrow(Ex);
      else
        warning(ex_formatted);
      end
    end % }}}
  end
end
