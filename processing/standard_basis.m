function standard_basis(mode)

  if nargin<1
    mode=[];
  end

  suffix='';
  noMM=0;

  if ~isempty(mode)
    if strcmp(mode,'noMM')
      suffix='_noMM';
      noMM=1;
    end
  end

  if ~exist('osp_addDiffMMPeaks')
    setup_submodules
  end
  force_FIDA('osprey')

  vendors={'ge','siemens','philips'};

  working_folder=[ '/home/alex/working/bigPRESS/data/basis' suffix ];

  quick=0;
  skip_shift=0;
  do_plotraw=1;
  do_figure=1;
  do_conversions=1;

  if quick
    do_plotraw=0;
    do_conversions=0;
  end;

  if do_figure
    close all;
  end

  scaling=struct('npts',{0,2048,4096,4096},'dwell',{0,0.5,0.2,0.25},'vendors',{{'ge','siemens','philips'},{'ge','philips'},{'ge'},{'siemens'}});
  subspecs={'off','diff'};

  if quick
    scaling=struct('npts',{0,2048,4096,4096},'dwell',{0,0.5,0.2,0.5},'vendors',{{'ge'},{'ge'},{'ge'},{'ge'}});
    subspecs={'diff'};
  end


  for is=1:length(scaling)

    this_scaling=scaling(is);
    vendors=this_scaling.vendors;
    this_scaling_tag='';

    for iss=1:length(subspecs)
      subspec=subspecs{iss};


      for iv=1:length(vendors) % {{{

        vendor=vendors{iv};

        fn_in=sprintf('../submodules/osprey/fit/basissets/3T/%s/mega/press/gaba68/basis_%s_megapress_gaba68.mat',vendor,vendor);
        load(fn_in,'BASIS')

        Bo=BASIS.Bo;
        HZPPPM=42.577*Bo;
        FWHMBA = BASIS.linewidth/HZPPPM;
        ECHOT = BASIS.te;
          
        BADELT=BASIS.dwelltime;
        NDATAB= BASIS.sz(1);
        SEQ='mega';

        if (this_scaling.npts>0)
          BADELT=this_scaling.dwell/1e3;
          NDATAB=this_scaling.npts;
        end

        sw_out=(1/BADELT);  % BADELT*NDATAB*1e3;
        this_scaling_tag=sprintf('%s_%i_%i',subspec,round(NDATAB),round(sw_out));

        disp(sprintf('%s : %s : BASIS %.6f / %d, scaling %.3f %d,  BADELT %.6f NDATAB %d',vendor, this_scaling_tag, BASIS.dwelltime,BASIS.sz(1),this_scaling.npts,this_scaling.dwell,BADELT,NDATAB));

        disp(sprintf('%s : %s', vendor,this_scaling_tag));

        vendor_working_folder=fullfile(working_folder,sprintf('%s_%d_%d',vendor,round(NDATAB),round(sw_out))); %vendor,this_scaling_tag);

        if ~exist(vendor_working_folder)
          mkdir(vendor_working_folder)
        end

        vendor_working_folder_jmrui=fullfile(working_folder,sprintf('jmrui_%s_%s_%d_%d',subspec,vendor,round(NDATAB),round(sw_out)));

        if ~exist(vendor_working_folder_jmrui)
          mkdir(vendor_working_folder_jmrui)
        end

        XTRASH = 0;

        txfrq=BASIS.Bo*42.577*1e6;
        %dwelltime=1/8192; % /(5000*1e3);% .00025

        basis={}
        basis{end+1}=' $SEQPAR';
        basis{end+1}=sprintf(' FWHMBA = %5.6f,',FWHMBA);
        basis{end+1}=sprintf(' HZPPPM = %5.6f,',HZPPPM);
        basis{end+1}=sprintf(' ECHOT = %2.2f,',ECHOT);
        basis{end+1}=sprintf(' SEQ = ''%s''',SEQ);
        basis{end+1}=sprintf(' $END');
        basis{end+1}=sprintf(' $BASIS1');
        basis{end+1}=sprintf(' IDBASI = ''%s'',',[vendor ' ' SEQ ' ' num2str(ECHOT) ' ms Osprey']);
        basis{end+1}=sprintf(' FMTBAS = ''(2E15.6)'',');
        basis{end+1}=sprintf(' BADELT = %5.6f,',BADELT);
        basis{end+1}=sprintf(' NDATAB = %i', NDATAB);
        basis{end+1}=sprintf(' $END');

        makebasis={}
        makebasis{end+1}=' $seqpar';
        makebasis{end+1}=' seq=''PRESS''';
        makebasis{end+1}=' echot=68.'; % previously incorrectly 68000
        makebasis{end+1}=' fwhmba=0.013';
        makebasis{end+1}=' $end';

        makebasis_fn=fullfile(vendor_working_folder,[subspec '.makebasis.in']);
        makebasis_fn_ps=fullfile(vendor_working_folder,[subspec '.basis.ps']);
        makebasis_fn_pdf=fullfile(vendor_working_folder,[subspec '.basis.pdf']);
        makebasis_fn_basis=fullfile(working_folder,[vendor '_' this_scaling_tag '.basis']);
        makebasis{end+1}='';
        makebasis{end+1}=' $nmall';
        makebasis{end+1}=sprintf(' hzpppm=%.2f',HZPPPM);
        makebasis{end+1}=sprintf(' deltat=%.5f',BADELT);
        makebasis{end+1}=sprintf(' nunfil=%d',NDATAB);
        makebasis{end+1}=sprintf(' filbas=''%s''',makebasis_fn_basis);
        makebasis{end+1}=sprintf(' filps=''%s''',makebasis_fn_ps);
        makebasis{end+1}=' autosc=.false.';
        makebasis{end+1}=' autoph=.false.';
        makebasis{end+1}=' consistent_scaling=.false.';
        makebasis{end+1}=sprintf(' idbasi=''ARC: 2020-12a Test basis set exported from osprey via fida, %s''', vendor);
        makebasis{end+1}=' $end';

        % ./libraries/FID-A/fitTools/fitModels/Osprey/osp_addDiffMMPeaks.m 
        %  fit/osp_fitMEGA.m 

        % coMM3 : 1to1GABA 3to2MM 3to2MMsoft 

        basisSet = BASIS;

        % need basisSetOff even in DIFF case, for correct scaling of MMco3
        basisSetOff=BASIS;
        basisSetOff.fids = basisSetOff.fids(:,:,1);
        basisSetOff.specs = basisSetOff.specs(:,:,1);

        % off: 1, diff: 3, sum: 4
        %                 [basisSet] = osp_addDiffMMPeaks(basisSet,fitOpts);
        if strcmp(subspec,'diff')
          basisSet.fids = basisSet.fids(:,:,3);
          basisSet.specs = basisSet.specs(:,:,3);

          fitOpts.coMM3='1to1GABAsoft';
          fitOpts.FWHMcoMM3=14;

          [basisSet] = osp_addDiffMMPeaks(basisSet,basisSetOff,fitOpts);

          if quick
            keep={'GABA','NAA','MM09'}
          else
            if noMM
              keep={'GABA','Glu','Gln','GSH','NAA','NAAG',        'MM09'} % HCar missing for GE; therefore, skip for all
            else
              keep={'GABA','Glu','Gln','GSH','NAA','NAAG','MM3co','MM09'} % HCar missing for GE; therefore, skip for all
            end
          end
          rename=containers.Map({'MM09'},{'MM09ex'}); % MM09 Conflicts with LCModel simulated MM basis set, and somehow vanishes with NSIMUL=0
        else
          basisSet=basisSetOff;
          basisSet.name
          % 2021-03-23: "default" basis set is not the full one! fix this!
          keep={'Asc', 'Asp', 'Cr', 'CrCH2', 'GABA', 'Gln', 'Glu', 'GPC', 'GSH', 'H2O', 'Ins', 'Lac', 'NAA', 'NAAG', 'PCh', 'PCr', 'PE', 'Scyllo', 'Tau', 'Tyros', 'Lip09', 'Lip13', 'Lip20', 'MM09', 'MM12', 'MM14', 'MM17', 'MM20'};
          rename=[];
        end
        
      %     idx_GABA          = find(strcmp(BASISdiff.name,'GABA'));

        conj_messed_up=0;

        if 0 || this_scaling.dwell>0

          % fit_resampleBasis takes ppm' and Bo from the first argument.
          fake_target=struct();
          fake_target.Bo=basisSet.Bo;

          % f=[(-sw_out/2)+(sw_out/(2*this_scaling.npts)) : this_scaling.dwell : (sw_out/2)-(sw_out/(2*this_scaling.npts))];

          f=[(-sw_out/2)+(sw_out/(2*NDATAB)) : (sw_out/NDATAB) : (sw_out/2)-(sw_out/(2*NDATAB))];

          ppm=f/(fake_target.Bo*42.577);

          fake_target.ppm=ppm+basisSet.centerFreq;

          sw_out
          this_scaling.npts
          if this_scaling.npts==0 && (length(fake_target.ppm)==length(basisSet.ppm))
            figure();
            subplot(1,2,1);
            plot(fake_target.ppm,basisSet.ppm)
            subplot(1,2,2);
            plot(fake_target.ppm-min(fake_target.ppm),basisSet.ppm-min(basisSet.ppm))
            xlabel('fake ppm');
            ylabel('basisSet ppm')
            drawnow();
            basisSet
            disp('fake delta ppm, dwell')
            fake_target.ppm(2)-fake_target.ppm(1)
            % fake_target.t(2)-fake_target.t(1)
            %fake_target.dwelltime
            % fake_target.spectralwidth
            this_scaling.dwell/1e3

            % f = np.linspace(0, 1 / T, N), where T= delta T
            approx_f_range=(1/BADELT) % /NDATAB

            sw_out
            disp('delta ppm,delta t,dwell,spectralwidth,sw from dwell?')
            basisSet.ppm(2)-basisSet.ppm(1)
            basisSet.t(2)-basisSet.t(1)
            basisSet.dwelltime
            basisSet.spectralwidth
            basisSet.dwelltime*basisSet.sz(1)

            % error('stop');
          end

          [basisSetScaled] = fit_resampleBasis(fake_target, basisSet); 

          % ../submodules/osprey/libraries/FID-A/fitTools/fit_resampleBasis.m

          basisSet
          basisSetScaled
          BADELT
          disp(sprintf('%.1f,%.1f,d', min(f), max(f), length(f)));
          disp(sprintf('%.1f,%.1f,d', min(ppm), max(ppm), length(ppm)));
          this_scaling_tag

          x_dppm                        = abs(basisSetScaled.ppm(2)-basisSetScaled.ppm(1))
          x_dppm                        = abs(fake_target.ppm(2)-fake_target.ppm(1))
          x_ppmrange                    = abs((basisSetScaled.ppm(end)-basisSetScaled.ppm(1)))+x_dppm

          x_spectralwidth   = x_ppmrange*fake_target.Bo*42.577
          sw_out

          x_dwelltime       = 1/x_spectralwidth
          BADELT

          % wait, what... does fit_resampleBasis miscalculate the dwell time? replace wit what we believe to be correct...

          if basisSetScaled.dwelltime~=BADELT
            warning('fit_resampleBasis returned a peculiar dwell time. attempting to fix....');
            basisSetScaled.dwelltime       = BADELT;
            basisSetScaled.t = (0:basisSetScaled.dwelltime:(basisSetScaled.sz(1)-1)*basisSetScaled.dwelltime);
            disp(sprintf('new dwell %.5f t min %.2f %.2f n %d', basisSetScaled.dwelltime, min(basisSetScaled.t),max(basisSetScaled.t),length(basisSetScaled.t)));
          end

          basisSet=basisSetScaled

          conj_messed_up=1;
          % fit_resampleBasis introduces a conjugate-sense inversion?! this is truly annoying.
        end


        for ik=1:length(keep)
          metab=keep{ik};
          metab_out=metab;

          if ~isempty(rename) && rename.isKey(metab_out)
            metab_out=rename(metab)
          end

          this_basis=basisSet;

          ix_met=find(strcmp(this_basis.name,metab));
          if sum(ix_met)==0
            this_basis.name
            warning([ 'Metabolite not found: ' metab]);
            continue
          end

          disp(sprintf('%s %s : %s => %s',vendor,this_scaling_tag, metab, metab_out));

          this_basis.geometry.size=struct('dim1',30,'dim2',30,'dim3',30);
          this_basis.fids = this_basis.fids(:,ix_met);

          this_basis.specs = this_basis.specs(:,ix_met);

          if conj_messed_up
            this_basis.fids=ifft(ifftshift(this_basis.specs));
                      %fids_interp(:,ll,rr)   = ifft(fftshift(specs_interp(:,ll,rr),1),[],1);
          end

          this_basis.seq='mega';
          this_basis.txfrq=txfrq;

          %size(this_basis.fids)

          output=fullfile(vendor_working_folder, sprintf('diff.%s.RAW', metab_out));

          if skip_shift
            this_basis_shifted=this_basis;
          else
            this_basis_shifted=shift_centerFreq(this_basis,1);
          end

          %this_basis_shifted_RF=shift_centerFreq_RF(this_basis,1);

          io_writelcm(this_basis_shifted,output,68000);

          output_jmrui=fullfile(vendor_working_folder_jmrui, sprintf('%s', metab_out)); % un-adorned metabolite name, so we can do a direct export-to-mrui format and have good names. import explicitly as text
          this_basis_shifted.date=datestr(now,'yyyymmdd');
          % this_basis_jmrui=shift_centerFreq(this_basis,4.7);
          io_writejmrui(this_basis_shifted,output_jmrui);
          % need to set reference explicitly within jmrui still

          if do_plotraw
            plotraw_ps_fn=fullfile(vendor_working_folder,['plotraw.' metab_out '.' subspec '.ps']);
            plotraw_pdf_fn=fullfile(vendor_working_folder,['plotraw.' metab_out '.' subspec '.pdf']);
            plotraw_png_fn=fullfile(vendor_working_folder,['plotraw.' metab_out '.' subspec '.png']);

            plotraw_in={};
            plotraw_in{end+1}=' $PLTRAW';

            plotraw_in{end+1}=sprintf(' hzpppm=%.2f',HZPPPM);
            plotraw_in{end+1}=sprintf(' deltat=%.5f',BADELT);
            plotraw_in{end+1}=sprintf(' nunfil=%d',NDATAB);

            plotraw_in{end+1}=sprintf(' FILRAW=''%s''',output);
            plotraw_in{end+1}=sprintf(' FILPS=''%s''',plotraw_ps_fn);
            plotraw_in{end+1}=' DEGPPM=-0.';
            plotraw_in{end+1}=' DEGZER=0.';
            plotraw_in{end+1}=' PPMEND=-1.';
            plotraw_in{end+1}=' PPMST=5.';
            plotraw_in{end+1}=' $END';

            plotraw_in_fn=fullfile(vendor_working_folder,['plotraw.' metab_out '.' subspec '.in']);
            f=fopen(plotraw_in_fn,'wt');
            fwrite(f,sprintf('%s\n',plotraw_in{:}));
            fclose(f);

            system(['~/.lcmodel/bin/plotraw < ' plotraw_in_fn]);
            if do_conversions
              system(['ps2pdf ' plotraw_ps_fn ' ' plotraw_pdf_fn ' >/dev/null 2>&1']);
              system(['convert ' plotraw_pdf_fn '[1] -background white -alpha remove ' plotraw_png_fn ' >/dev/null 2>&1']);
            end
          end


          makebasis{end+1}='';
          makebasis{end+1}=' $nmeach';
          makebasis{end+1}=sprintf(' filraw=''%s''',output);
          makebasis{end+1}=sprintf(' metabo=''%s''', metab_out);
          makebasis{end+1}=' degzer=0.';
          makebasis{end+1}=' degppm=0.';
          makebasis{end+1}=' conc=1.';
          makebasis{end+1}=' noshif=.true.';
          makebasis{end+1}=' ppmapp=0.2, -.2'; % despite noshift, still have to enter sensible values. these won't be used
          makebasis{end+1}=' $end';


          if do_figure 
            if ik==1
              figure();
            end

            range=[0,4.5];

            subplot(2,3,1);
            fmask=this_basis.ppm>min(range) & this_basis.ppm<max(range);
            fmask=this_basis.ppm<999;
            plot(this_basis.ppm(fmask),real(this_basis.specs(fmask)));

            if ik==1
              hold on;
              title([ vendor ' ' this_scaling_tag ]);
            end;

            subplot(2,3,2);

            fmask=this_basis_shifted.ppm>min(range) & this_basis_shifted.ppm<max(range);
            fmask=this_basis_shifted.ppm<999;
            plot(this_basis_shifted.ppm(fmask),real(this_basis_shifted.specs(fmask)));

            if ik==1
              hold on;
              title([ vendor ' ' this_scaling_tag ' shifted']);
            end;

            subplot(2,3,3);

            plot(real(fftshift(fft(this_basis.fids(:)))));

            if ik==1
              hold on;
              title([ vendor ' ' this_scaling_tag ' fft']);
            end;

            subplot(2,3,4);

            plot(real(fftshift(fft(this_basis_shifted.fids(:)))));

            if ik==1
              hold on;
              title([ vendor ' ' this_scaling_tag ' shifted fft']);
            end;

            if ik==1
              subplot(2,3,5);
              plot(this_basis.t,real(this_basis.fids(:)),'b'); hold on;
              plot(this_basis.t,imag(this_basis.fids(:)),'g');
              xlim([0,1])

              title('fid unshifted');
              subplot(2,3,6);
              plot(this_basis_shifted.t,real(this_basis_shifted.fids(:)),'b'); hold on;
              plot(this_basis_shifted.t,imag(this_basis_shifted.fids(:)),'g--');
              title('fid shifted');
              xlim([0,1])
            end


          end % do_figure

          
        end % per metab

        drawnow();

        f=fopen(makebasis_fn,'wt');
        fwrite(f,sprintf('%s\n',makebasis{:}));
        fclose(f);

        system([ '/home/alex/.lcmodel/bin/makebasis < ' makebasis_fn ])
        system([ 'ps2pdf ' makebasis_fn_ps ' ' makebasis_fn_pdf ]);

      end % vendors }}}
    end % subspec
  end % scaling

end % function standard_basis

% refer also: ../submodules/osprey/libraries/FID-A/inputOutput/io_writelcmBASIS.m
% refer also: io_writeTarquinBASIS

function [RF] = shift_centerFreq_RF(data_struct,idx,do_conj)

    if nargin<3
      do_conj=1;
    end

    t=repmat(data_struct.t',[1 data_struct.sz(2:end,1)]);
    hzpppm = data_struct.Bo*42.577;
    f = (4.68-data_struct.centerFreq)*hzpppm;
    fids = data_struct.fids(:,idx);
    fids=fids.*exp(-1i*t*f*2*pi);
    %Take the complex conjugate becuase the sense of rotation in LCModel seems to
    %be opposite to that used in FID-A.
    if do_conj
      fids = conj(fids);
    end
    specs=(fft(fids,[],data_struct.dims.t));
    RF=zeros(length(specs(:)),2);
    RF(:,1)=real(specs(:));
    RF(:,2)=imag(specs(:));

end

function data_struct = shift_centerFreq(data_struct,idx,do_conj)
    if nargin<3
      do_conj=0;
    end

    t=repmat(data_struct.t',[1 data_struct.sz(2:end,1)]);
    hzpppm = data_struct.Bo*42.577;
    f = (4.68-data_struct.centerFreq)*hzpppm;
    fids = data_struct.fids(:,idx);
    fids=fids.*exp(-1i*t*f*2*pi);

    %Take the complex conjugate becuase the sense of rotation in LCModel seems to
    %be opposite to that used in FID-A.
    if do_conj
      fids = conj(fids);
    end
    specs=(fft(fids,[],data_struct.dims.t));

    data_struct.fids=fids;
    data_struct.specs=specs;

end
