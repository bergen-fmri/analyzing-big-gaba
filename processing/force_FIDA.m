function force_FIDA(mode)

 % switch between 'standard'-ish upstream FID-A version, and Osprey's adapted version

  p=mfilename('fullpath');
  [base,x_,xx_]=fileparts(p);
  [parent,x_,xx_]=fileparts(base);
  submodule_folder=fullfile(parent,'submodules');

  pp=split(path(),':');
  flush_bits={'FID-A'};
  for i=1:length(pp)
    for xi=1:length(flush_bits)
      if ~isempty(strfind(pp{i},flush_bits{xi}))
        disp(sprintf('Removing %s', pp{i}))
        rmpath(pp{i})
        break;
      end
    end
  end

  addpath(genpath(fullfile(submodule_folder,'osprey/libraries/FID-A'))); % Add osprey FID-A as fallback, but maybe load 'compat' over the top...

  if strcmp(mode,'compat') 
    % use upstream/"compatible" FID-A version. specs are flipped in the conjugate sense
    bits={'FID-A','FID-A/inputOutput','FID-A/processingTools', 'FID-A/inputOutput/gannetTools', 'FID-A/processingTools/phase'};
    for bit=bits
      bit=bit{1};
      b=fullfile(submodule_folder,bit);
      if exist(b)
        disp(sprintf('Adding %s', b))
        addpath(b);
      else
        warning(sprintf('Missing %s', b));
      end
    end
  elseif strcmp(mode,'osprey')
    addpath(genpath(fullfile(submodule_folder,'osprey/libraries/FID-A')));
  else
    errror('force_FIDA: unknown mode');
  end
end
